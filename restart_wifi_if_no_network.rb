#!/usr/bin/env ruby
STDOUT.sync=true

CHECK_HOST = "google.nl"

def check_connection
  system("ping #{CHECK_HOST} -c 5 1> /dev/null 2>/dev/null")
  return true if $? == 0
end

loop do
  print "[#{Time.now.to_s}] Checking connection (check if #{CHECK_HOST.inspect} is accessible) ... "
  if check_connection
    print " Connection OK."
  else
    print " Connection broken! Restarting network ... "
    `service network-manager restart`
    print " restarted. Sleep 10 extra seconds. "
    sleep 10
  end

  puts "Sleeping 5 seconds"
  sleep 5
end